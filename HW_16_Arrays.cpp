
#include <iostream>
#include <time.h>

int main()
{
    setlocale(LC_ALL, "Russian");

    const int N = 5;
    int array[N][N];

    std::cout << "����� ���������� �������:\n" << std::endl;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            array[i][j] = i + j;
            std::cout << array[i][j];
        }
        std::cout << std::endl;
    }
    for (int i = 0; i < 40; ++i) {
        std::cout << "_";
    }
    std::cout << std::endl;

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int rowIndex = buf.tm_mday % N;
    int rowSum = 0;

    for (int j = 0; j < N; ++j)
    {
        rowSum += array[rowIndex][j];
    }

    std::cout << "����� ��������� ������� � �������� " << rowIndex << ":\n" << std::endl;
    std::cout << rowSum << std::endl;

    return 0;
}



